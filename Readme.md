# Wordpress Challenge

## This repository contains a wordpress technical challenge.

## Objective
Create a small ecommerce store to sell different kinds of products.



## Installation

Clone the repository to your desire path:

```bash
git clone https://bitbucket.org/loire_euceda/as-wordpress-challenge/src/master/ YOUR_DIR_NAME
```

Inside the project root folder create a docker-compose.override.yml file and customize the following data according to your project path.

**Windows Example:**

```yml
version: '3.7'

services:
  mariadb:
    volumes:
      - /c/Users/Alicia/wp-challenge/db_data:/var/lib/mysql
      - /c/Users/Alicia/wp-challenge/db_dump:/docker-entrypoint-initdb.d

  wordpress:
    volumes:
      - /c/Users/Alicia/wp-challenge/src:/var/www/html
```

**Unix Example:**

```yml
version: '3.7'

services:
  mariadb:
    volumes:
      - ./db_data:/var/lib/mysql
      - ./db_dump:/docker-entrypoint-initdb.d

  wordpress:
    volumes:
      - ./src:/var/www/html
```

## Download images and run the containers

### Starting the DB container with the following command:

```bash
docker-compose up -d mariadb
```

You need to wait about 5 minutes for mariadb container to finish the imports, once all is set then you can continue:

### Starting the Wordpress container:

```bash
docker-compose up -d wordpress
```

Now you can enter to:

```bash
http://localhost:8000/
```

## Optional

### Starting a DB client: Adminer

you can access to the DB using this DB client container:

```bash
docker-compose up -d adminer
```