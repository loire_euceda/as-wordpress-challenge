<?php
/**
 * Login Form
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>

<div class="u-columns col2-set" id="customer_login">

	<div class="u-column1 col-1">

<?php endif; ?>

        <div class="login-content">
            <h2><?php esc_html_e( 'Login', 'woocommerce' ); ?> | <?php esc_html_e( 'Sign up', 'woocommerce' ); ?></h2>
            <a class="custom-oauth-button" href="javascript:void(0)" onclick="moOAuthLoginNew('customApp');">
                <div class="mo_oauth_login_button_widget">
                    <i class="fa fa-lock mo_oauth_login_button_icon_widget"></i><h3 class="mo_oauth_login_button_text_widget">Continue with Splitwise</h3>
                </div>
            </a>
        </div>

        <script type="text/javascript">
            function HandlePopupResult(result) {
                window.location.href = result;
            }
            function moOAuthLoginNew(app_name) {
                window.location.href = '<?php echo site_url() ?>' + '/?option=oauthredirect&app_name=' + app_name;
            }
        </script>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
