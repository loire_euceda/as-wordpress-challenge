<?php
/**
 * The template for displaying the about me.
 *
 * Template name: About me
 *
 */

get_header(); ?>

    <div id="content" class="site-content" tabindex="-1">
        <div class="col-full">

            <div class="woocommerce"></div>
            <div id="primary" class="content-area content-about">
                <main id="main" class="site-main" role="main">
                    <article id="post-18" class="post-18 page type-page status-publish hentry">
                        <div class="entry-content">
                            <div class="wp-block-columns">
                                <div class="wp-block-column">
                                    <figure class="wp-block-image size-large is-resized"><img loading="lazy" src="http://localhost:8000/wp-content/uploads/2021/02/IMG_20181027_204516-768x1024.jpg" alt="" class="wp-image-21" width="384" height="512" srcset="http://localhost:8000/wp-content/uploads/2021/02/IMG_20181027_204516-768x1024.jpg 768w, http://localhost:8000/wp-content/uploads/2021/02/IMG_20181027_204516-225x300.jpg 225w, http://localhost:8000/wp-content/uploads/2021/02/IMG_20181027_204516-1152x1536.jpg 1152w, http://localhost:8000/wp-content/uploads/2021/02/IMG_20181027_204516-1536x2048.jpg 1536w, http://localhost:8000/wp-content/uploads/2021/02/IMG_20181027_204516-416x555.jpg 416w, http://localhost:8000/wp-content/uploads/2021/02/IMG_20181027_204516-scaled.jpg 1920w" sizes="(max-width: 384px) 100vw, 384px"></figure>
                                </div>
                                <div class="wp-block-column">
                                    <h1 class="has-text-color"><strong>About Me</strong></h1>
                                    <p>Hi, I’m Alicia.<br>I’m a web developer (in development) living in San Pedro Sula Honduras. I graduated from Computer Science and I’ve been working developing applications for the web using mostly PHP, but picking up a new framework or language isn’t a problem. I do this for a living and love what I do as every day there is something new and exciting to learn.</p>
                                    <div class="wp-block-buttons">
                                        <div class="wp-block-button is-style-fill"><a class="wp-block-button__link" href="https://www.linkedin.com/in/aliciaeuceda/" target="_blank" rel="noreferrer noopener">View full details on my Linkedin</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wp-block-columns">
                                <div class="wp-block-column">
                                    <h2 class="has-text-align-center">Stuff I Love</h2>
                                </div>
                            </div>
                            <figure class="wp-block-gallery columns-3 is-cropped"><ul class="blocks-gallery-grid"><li class="blocks-gallery-item"><figure><img loading="lazy" width="1024" height="576" src="http://localhost:8000/wp-content/uploads/2021/02/Horizon-Zero-dawn-1024x576.jpg" alt="" data-id="39" data-full-url="http://localhost:8000/wp-content/uploads/2021/02/Horizon-Zero-dawn.jpg" data-link="http://localhost:8000/about-me/horizon-zero-dawn/" class="wp-image-39" srcset="http://localhost:8000/wp-content/uploads/2021/02/Horizon-Zero-dawn-1024x576.jpg 1024w, http://localhost:8000/wp-content/uploads/2021/02/Horizon-Zero-dawn-300x169.jpg 300w, http://localhost:8000/wp-content/uploads/2021/02/Horizon-Zero-dawn-768x432.jpg 768w, http://localhost:8000/wp-content/uploads/2021/02/Horizon-Zero-dawn-1536x864.jpg 1536w, http://localhost:8000/wp-content/uploads/2021/02/Horizon-Zero-dawn-416x234.jpg 416w, http://localhost:8000/wp-content/uploads/2021/02/Horizon-Zero-dawn.jpg 1920w" sizes="(max-width: 1024px) 100vw, 1024px"><figcaption class="blocks-gallery-item__caption">Video games</figcaption></figure></li><li class="blocks-gallery-item"><figure><img loading="lazy" width="616" height="770" src="http://localhost:8000/wp-content/uploads/2021/02/IMG_20191001_151929_527.jpg" alt="" data-id="40" data-full-url="http://localhost:8000/wp-content/uploads/2021/02/IMG_20191001_151929_527.jpg" data-link="http://localhost:8000/about-me/img_20191001_151929_527/" class="wp-image-40" srcset="http://localhost:8000/wp-content/uploads/2021/02/IMG_20191001_151929_527.jpg 616w, http://localhost:8000/wp-content/uploads/2021/02/IMG_20191001_151929_527-240x300.jpg 240w, http://localhost:8000/wp-content/uploads/2021/02/IMG_20191001_151929_527-416x520.jpg 416w" sizes="(max-width: 616px) 100vw, 616px"><figcaption class="blocks-gallery-item__caption">Travel</figcaption></figure></li><li class="blocks-gallery-item"><figure><img loading="lazy" width="1024" height="683" src="http://localhost:8000/wp-content/uploads/2021/02/lazy-morning-programming-in-a-bed-2210x1473-1-1024x683.jpg" alt="" data-id="41" data-full-url="http://localhost:8000/wp-content/uploads/2021/02/lazy-morning-programming-in-a-bed-2210x1473-1.jpg" data-link="http://localhost:8000/about-me/lazy-morning-programming-in-a-bed-2210x1473-1/" class="wp-image-41" srcset="http://localhost:8000/wp-content/uploads/2021/02/lazy-morning-programming-in-a-bed-2210x1473-1-1024x683.jpg 1024w, http://localhost:8000/wp-content/uploads/2021/02/lazy-morning-programming-in-a-bed-2210x1473-1-300x200.jpg 300w, http://localhost:8000/wp-content/uploads/2021/02/lazy-morning-programming-in-a-bed-2210x1473-1-768x512.jpg 768w, http://localhost:8000/wp-content/uploads/2021/02/lazy-morning-programming-in-a-bed-2210x1473-1-1536x1024.jpg 1536w, http://localhost:8000/wp-content/uploads/2021/02/lazy-morning-programming-in-a-bed-2210x1473-1-2048x1365.jpg 2048w, http://localhost:8000/wp-content/uploads/2021/02/lazy-morning-programming-in-a-bed-2210x1473-1-416x277.jpg 416w" sizes="(max-width: 1024px) 100vw, 1024px"><figcaption class="blocks-gallery-item__caption">Programing. At least when things go well</figcaption></figure></li><li class="blocks-gallery-item"><figure><img loading="lazy" width="1024" height="1024" src="http://localhost:8000/wp-content/uploads/2021/02/OQ5DFJ0-1024x1024.jpg" alt="" data-id="42" data-full-url="http://localhost:8000/wp-content/uploads/2021/02/OQ5DFJ0.jpg" data-link="http://localhost:8000/about-me/oq5dfj0/" class="wp-image-42" srcset="http://localhost:8000/wp-content/uploads/2021/02/OQ5DFJ0-1024x1024.jpg 1024w, http://localhost:8000/wp-content/uploads/2021/02/OQ5DFJ0-300x300.jpg 300w, http://localhost:8000/wp-content/uploads/2021/02/OQ5DFJ0-150x150.jpg 150w, http://localhost:8000/wp-content/uploads/2021/02/OQ5DFJ0-768x768.jpg 768w, http://localhost:8000/wp-content/uploads/2021/02/OQ5DFJ0-1536x1536.jpg 1536w, http://localhost:8000/wp-content/uploads/2021/02/OQ5DFJ0-324x324.jpg 324w, http://localhost:8000/wp-content/uploads/2021/02/OQ5DFJ0-416x416.jpg 416w, http://localhost:8000/wp-content/uploads/2021/02/OQ5DFJ0-100x100.jpg 100w, http://localhost:8000/wp-content/uploads/2021/02/OQ5DFJ0.jpg 2000w" sizes="(max-width: 1024px) 100vw, 1024px"><figcaption class="blocks-gallery-item__caption">My cats <img draggable="false" role="img" class="emoji" alt="🐱" src="https://s.w.org/images/core/emoji/13.0.1/svg/1f431.svg"><img draggable="false" role="img" class="emoji" alt="🐱" src="https://s.w.org/images/core/emoji/13.0.1/svg/1f431.svg"></figcaption></figure></li><li class="blocks-gallery-item"><figure><img loading="lazy" width="420" height="280" src="http://localhost:8000/wp-content/uploads/2021/02/pexels-cottonbro-3944091.jpg" alt="" data-id="43" data-full-url="http://localhost:8000/wp-content/uploads/2021/02/pexels-cottonbro-3944091.jpg" data-link="http://localhost:8000/about-me/pexels-cottonbro-3944091/" class="wp-image-43" srcset="http://localhost:8000/wp-content/uploads/2021/02/pexels-cottonbro-3944091.jpg 420w, http://localhost:8000/wp-content/uploads/2021/02/pexels-cottonbro-3944091-300x200.jpg 300w, http://localhost:8000/wp-content/uploads/2021/02/pexels-cottonbro-3944091-416x277.jpg 416w" sizes="(max-width: 420px) 100vw, 420px"><figcaption class="blocks-gallery-item__caption">Music</figcaption></figure></li><li class="blocks-gallery-item"><figure><img loading="lazy" width="420" height="630" src="http://localhost:8000/wp-content/uploads/2021/02/pexels-oziel-gomez-2846814.jpg" alt="" data-id="44" data-full-url="http://localhost:8000/wp-content/uploads/2021/02/pexels-oziel-gomez-2846814.jpg" data-link="http://localhost:8000/about-me/pexels-oziel-gomez-2846814/" class="wp-image-44" srcset="http://localhost:8000/wp-content/uploads/2021/02/pexels-oziel-gomez-2846814.jpg 420w, http://localhost:8000/wp-content/uploads/2021/02/pexels-oziel-gomez-2846814-200x300.jpg 200w, http://localhost:8000/wp-content/uploads/2021/02/pexels-oziel-gomez-2846814-416x624.jpg 416w" sizes="(max-width: 420px) 100vw, 420px"><figcaption class="blocks-gallery-item__caption">Read</figcaption></figure></li></ul></figure>
                            <div style="height:80px" aria-hidden="true" class="wp-block-spacer"></div>
                            <div class="wp-block-columns">
                                <div class="wp-block-column">
                                    <h2 class="has-text-align-center">Facts About Me</h2>
                                    <p></p>
                                </div>
                            </div>
                            <div class="wp-block-columns">
                                <div class="wp-block-column">
                                    <ul><li>Favorite smell? Vanilla <img draggable="false" role="img" class="emoji" alt="💛" src="https://s.w.org/images/core/emoji/13.0.1/svg/1f49b.svg"></li><li>I have 3 brothers. Yes I’m the only girl <img draggable="false" role="img" class="emoji" alt="👧" src="https://s.w.org/images/core/emoji/13.0.1/svg/1f467.svg"></li><li>I can’t fall asleep if I don’t read before.</li><li>I lived in Ireland for three months <img draggable="false" role="img" class="emoji" alt="🍀" src="https://s.w.org/images/core/emoji/13.0.1/svg/1f340.svg"></li><li>My best time for coding is in the night <img draggable="false" role="img" class="emoji" alt="🌝" src="https://s.w.org/images/core/emoji/13.0.1/svg/1f31d.svg"></li><li>When I was 15 years old I wanted to develop video games.</li><li>Most of the time I listen to music while coding.</li></ul>
                                </div>
                                <div class="wp-block-column">
                                    <figure class="wp-block-image size-medium is-style-default"><img loading="lazy" width="300" height="197" src="http://localhost:8000/wp-content/uploads/2021/02/2020-11-28-01-39-10-606-300x197.jpg" alt="" class="wp-image-48" srcset="http://localhost:8000/wp-content/uploads/2021/02/2020-11-28-01-39-10-606-300x197.jpg 300w, http://localhost:8000/wp-content/uploads/2021/02/2020-11-28-01-39-10-606-1024x671.jpg 1024w, http://localhost:8000/wp-content/uploads/2021/02/2020-11-28-01-39-10-606-768x504.jpg 768w, http://localhost:8000/wp-content/uploads/2021/02/2020-11-28-01-39-10-606-416x273.jpg 416w, http://localhost:8000/wp-content/uploads/2021/02/2020-11-28-01-39-10-606.jpg 1371w" sizes="(max-width: 300px) 100vw, 300px"></figure>
                                </div>
                            </div>
                        </div><!-- .entry-content -->
                        <div class="edit-link"><a class="post-edit-link" href="http://localhost:8000/wp-admin/post.php?post=18&amp;action=edit">Edit <span class="screen-reader-text">About me</span></a></div></article><!-- #post-## -->
                </main><!-- #main -->
            </div><!-- #primary -->
        </div><!-- .col-full -->
    </div>
<?php

get_footer();
