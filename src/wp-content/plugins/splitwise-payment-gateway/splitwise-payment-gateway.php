<?php
/*
Plugin Name: WooCommerce Splitwise Gateway
Description: Extends WooCommerce with an Splitwise gateway.
Version: 1.0
Author: Alicia Euceda
*/
 
add_action('plugins_loaded', 'woocommerce_gateway_name_init', 0);
 
function woocommerce_gateway_name_init() {
 
	if (!class_exists('WC_Payment_Gateway')) return;

	/**
 	 * Localisation
	 */
	load_plugin_textdomain('wc-gateway-name', false, dirname(plugin_basename(__FILE__)) . '/languages');
    
	/**
 	 * Splitwise Gateway class
 	 */
	class WC_Gateway_Name extends WC_Payment_Gateway {
	
		public function __construct(){
			$this->id                 = 'splitwise';
			$this->icon               = apply_filters('woocommerce_cheque_icon', '');
			$this->has_fields         = false;
			$this->method_title       = __('Splitwise', 'woocommerce');
			$this->method_description = __('Allows charging expenses within Splitwise', 'woocommerce');

			// Load the settings.
			$this->init_form_fields();
			$this->init_settings();

			// Define user set variables
			$this->title        = $this->get_option('title');
			$this->description  = $this->get_option('description');
			$this->instructions = $this->get_option('instructions', $this->description);
			$this->api_key      = $this->get_option('api_key');
            $this->sw_account_email = $this->get_option('email');
			$this->resource_url = 'https://secure.splitwise.com/api/v3.0/create_expense';
			$this->notify_url   = str_replace('http:', 'https:', home_url('/wc-api/WC_Gateway_Name') );

			// Actions
			add_action('woocommerce_api_wc_gateway_name', 
				array($this, 'response_catcher'));
			add_action('woocommerce_update_options_payment_gateways_' . 
				    $this->id, array($this, 'process_admin_options'));
			add_action('woocommerce_thankyou_splitwise', array($this, 'thankyou_page'));

			// Customer Emails
			// add_action('woocommerce_email_before_order_table', array($this, 'email_instructions'), 10, 2);
		}

		/**
		* Initialise Gateway Settings Form Fields
		*/
		public function init_form_fields() {

		$this->form_fields = array(
		    'enabled' => array(
                'title'   => __('Enable/Disable', 'woocommerce'),
                'type'    => 'checkbox',
                'label'   => __('Enable Splitwise Payment', 'woocommerce'),
                'default' => 'yes'
		   ),
		    'title' => array(
                'title'       => __('Title', 'woocommerce'),
                'type'        => 'text',
                'description' => __('This controls the title which the user sees during checkout.', 'woocommerce'),
                'default'     => __('Splitwise Payment', 'woocommerce'),
                'desc_tip'    => true,
		   ),
		    'description' => array(
                'title'       => __('Description', 'woocommerce'),
                'type'        => 'textarea',
                'description' => __('Payment method description that the customer will see on your checkout.',
                          'woocommerce'),
                'default'     => __('The total amount of the order will be added as an expense to your Splitwise account', 'woocommerce'),
                'desc_tip'    => true,
		   ),
		    'instructions' => array(
                'title'       => __('Instructions', 'woocommerce'),
                'type'        => 'textarea',
                'description' => __('The total amount of the order will be added as an expense to your Splitwise account',
                          'woocommerce'),
			'default'     => '',
			'desc_tip'    => true,
            ),
            'email' => array(
                'title'       => __('Email', 'woocommerce'),
                'type'        => 'text',
                'description' => __('The email address of the Slipwise account'),
                'default'     => '',
                'desc_tip'    => true,
            ),
		    'api_key' => array(
                'title'       => __('API Key', 'woocommerce'),
                'type'        => 'text',
                'description' => __('API Key that will be use it as a Bearer token in the Authorization header when making requests to the Splitwise API in order to create the expenses'),
                'default'     => '',
                'desc_tip'    => true,
		   )
		);
		}

		/**
		* Output for the order received page.
		*/
		public function thankyou_page() {
			if ($this->instructions)
				echo wpautop(wptexturize($this->instructions));
		}

		/**
         * Process the payment and return the result
         **/
		function process_payment($order_id){

		    $order = new WC_Order($order_id);

		    // Add the expense as a debt to Splitwise Customer account
		    $this->add_expense($order);

            // Mark as on-hold (we're awaiting the payment)
            $order->update_status( 'on-hold', __( 'Awaiting offline payment', 'wc-gateway-offline' ) );

            // Reduce stock levels
            $order->reduce_order_stock();

            // Remove cart
            WC()->cart->empty_cart();

            // Return thankyou redirect
            return array(
                'result' 	=> 'success',
                'redirect'	=> $this->get_return_url( $order )
            );
		}

        /**
         * Add a expense to the current customer with the order info
         **/
        function add_expense($order) {
            $order_id = $order->get_id();
            $order_total = number_format($order->get_total(), 2, '.', '');
            $description = "Nero's Shop order #$order_id";

            $params = [
                'description'       => $description,
                'cost'              => $order_total,
                'users__0__email'   => $this->sw_account_email,
                'users__1__email'   => $order->get_billing_email(),
                'users__0__paid_share' => $order_total,
                'users__1__owed_share' => $order_total,
                'currency_code'        => 'USD',
            ];

            $final_resource_url = $this->resource_url . "?" . http_build_query($params);
            $this->getRequest(
                $final_resource_url,
                $this->api_key,
            );
        }

        /**
         * Create a request to API and return body content
         **/
        function getRequest($resource_url, $api_key){
            $response   = wp_remote_post( $resource_url, array(
                'method'      => 'POST',
                'timeout'     => 45,
                'redirection' => 5,
                'httpversion' => '1.0',
                'blocking'    => true,
                'headers'     => array(
                    'Authorization' => 'Bearer '.$api_key
                ),
                'cookies'     => array(),
                'sslverify'   => false
            ) );

            $response =  $response['body'];

            if(!is_array(json_decode($response, true))){
                $response = addcslashes($response, '\\');
                if(!is_array(json_decode($response, true))){
                    echo "<b>Response : </b><br>";print_r($response);echo "<br><br>";
                    exit("Invalid response received.");
                }
            }

            $content = json_decode($response,true);
            if(isset($content["error_description"])){
                exit($content["error_description"]);
            } else if(isset($content["error"])){
                exit($content["error"]);
            }

            return $content;
        }
	}
	
	/**
 	* Adding the Gateway to WooCommerce
 	**/
	function woocommerce_add_gateway_name_gateway($methods) {
		$methods[] = 'WC_Gateway_Name';
		return $methods;
	}
	
	add_filter('woocommerce_payment_gateways', 'woocommerce_add_gateway_name_gateway');
}
